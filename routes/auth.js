const express = require('express');
const router = express.Router();
const User = require('../models/User');

router.post('/register', async (req, res) => {
    try {
        const { username, email, password } = req.body;
        const newUser = new User({ username, email, password });
        await newUser.save();
        res.status(200).send('User registered successfully');
    } catch (err) {
        res.status(500).send('Error registering user');
    }
});

router.post('/login', async (req, res) => {
    const { username, email, password } = req.body;
    const user = await User.findOne({ $or: [{ username }, { email }] });
    if (user && (await user.comparePassword(password))) {
        req.session.userId = user._id;
        res.status(200).send('Login successful');
    } else {
        res.status(401).send('Authentication error');
    }
});

router.post('/logout', (req, res) => {
    req.session.destroy();
    res.status(200).send('Logout successful');
});

router.get('/users', async (req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    } catch (err) {
        console.error(err);
        res.status(500).send('Internal Server Error');
    }
});

router.delete('/delete/:userId', async (req, res) => {
    const userId = req.params.userId;

    try {
        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).send('User not found');
        }

        await User.findByIdAndDelete(userId);

        res.status(200).send('User deleted successfully');
    } catch (err) {
        res.status(500).send('Error deleting user');
    }
});

module.exports = router;
