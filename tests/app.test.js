const request = require('supertest');
const app = require('../app');
const mongoose = require('mongoose');

describe('Authentication and User Management Tests', () => {
  let agent;

  beforeAll(async () => {
    agent = request.agent(app);
    await mongoose.connect('mongodb://localhost:27017/authdemo_test', { useNewUrlParser: true, useUnifiedTopology: true });
  });

  afterAll(async () => {
    await mongoose.connection.db.dropDatabase();
    await mongoose.connection.close();
  });

  test('Successful registration', async () => {
    const response = await request(app)
      .post('/auth/register')
      .send({ username: 'newuser', email: 'newuser@example.com', password: 'Password123' });
    expect(response.statusCode).toBe(200);
  });

  test('Successful login', async () => {
    const response = await agent
      .post('/auth/login')
      .send({ username: 'newuser', password: 'Password123' });
    expect(response.statusCode).toBe(200);
  });

  test('Get all users', async () => {
    const response = await request(app)
      .get('/auth/users');
    expect(response.statusCode).toBe(200);
    expect(response.body).toBeInstanceOf(Array);
  });

  test('Delete user', async () => {
    await agent.post('/auth/register').send({ username: 'userToDelete', email: 'delete@example.com', password: 'Password123' });
    await agent.post('/auth/login').send({ username: 'userToDelete', password: 'Password123' });

    const userToDelete = await User.findOne({ username: 'userToDelete' });

    const deleteResponse = await agent.delete(`/auth/delete/${userToDelete._id}`);
    expect(deleteResponse.statusCode).toBe(200);
  });

  test('Successful logout', async () => {
    const response = await agent
      .post('/auth/logout');
    expect(response.statusCode).toBe(200);
  });
});
